<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class MaterializeAsset extends AssetBundle {

    public $sourcePath = '@frontend/assets/materialize';
    // public $baseUrl = '@web';
    public $css = [
//        'css/site.css',
        'css/materialize.min.css',
        'css/style.min.css',
        'css/layouts/style-fullscreen.css',
        'css/custom/custom.min.css',
        'js/plugins/perfect-scrollbar/perfect-scrollbar.css',
        'js/plugins/jvectormap/jquery-jvectormap.css',
        'js/plugins/chartist-js/chartist.min.css',
    ];
    public $js = [
//        'js/plugins/jquery-1.11.2.min.js',
        'js/materialize.min.js',
        'js/plugins/perfect-scrollbar/perfect-scrollbar.min.js',
        'js/plugins/chartist-js/chartist.min.js',
        'js/plugins/chartjs/chart.min.js',
        'js/plugins/sparkline/jquery.sparkline.min.js',
        'js/plugins/sparkline/sparkline-script.js',
        'js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js',
        'js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js',
        'js/plugins/jvectormap/vectormap-script.js',
        'js/plugins.min.js',
//        'js/custom-script.js',
    ];
    public $depends = [
//        'js/plugins/jquery-1.11.2.min.js',
//         'js/materialize.min.js',
        'yii\web\JqueryAsset',
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

}
