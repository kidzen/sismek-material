<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Data */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="data-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id', ['options' => ['class' => 'input-field']])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'geolocation', ['options' => ['class' => 'input-field']])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'site', ['options' => ['class' => 'input-field']])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'location', ['options' => ['class' => 'input-field']])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'type', ['options' => ['class' => 'input-field']])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'brand', ['options' => ['class' => 'input-field']])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'model', ['options' => ['class' => 'input-field']])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'horse_power', ['options' => ['class' => 'input-field']])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'serial_no', ['options' => ['class' => 'input-field']])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'kew_pa', ['options' => ['class' => 'input-field']])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'floor', ['options' => ['class' => 'input-field']])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status', ['options' => ['class' => 'input-field']])->textInput() ?>

    <?= $form->field($model, 'created_at', ['options' => ['class' => 'input-field']])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'updated_at', ['options' => ['class' => 'input-field']])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_by', ['options' => ['class' => 'input-field']])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'updated_by', ['options' => ['class' => 'input-field']])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'deleted', ['options' => ['class' => 'input-field']])->textInput() ?>

    <?= $form->field($model, 'deleted_at', ['options' => ['class' => 'input-field']])->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
