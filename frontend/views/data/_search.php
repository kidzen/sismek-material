<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\DataSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="data-search">

    <?php
    $form = ActiveForm::begin([
                'action' => ['index'],
                'method' => 'get',
    ]);
    ?>

    <?php echo $form->field($model, 'id') ?>

    <?php // echo $form->field($model, 'status', ['options' => ['class' => 'input-field']])->dropDownList([0, 1, 2]) ?>
    <div class="row">

        <div class="input-field field-datasearch-status">
            <label class="control-label" for="datasearch-status">Status</label>
            <div class="select-wrapper form-control initialized"><span class="caret">▼</span>
                <input type="text" class="select-dropdown" readonly="true" data-activates="select-options-8dcc4485-0961-a027-4ae5-525e9fcba333" value="0">
                <ul id="select-options-8dcc4485-0961-a027-4ae5-525e9fcba333" class="dropdown-content select-dropdown" style="width: 1194px; position: absolute; top: 6px; left: 12px; opacity: 1; display: none;">
                    <li class=""><span>0</span></li>
                    <li class=""><span>1</span></li><li class=""><span>2</span></li></ul><select id="datasearch-status" class="form-control initialized" name="DataSearch[status]">
                    <option value="0">0</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                </select></div>

            <div class="help-block"></div>
        </div>


        <div class="input-field col s12">
            <label>Materialize Select</label>
            <div class="select-wrapper initialized"><span class="caret">▼</span>
                <input type="text" class="select-dropdown" readonly="true" data-activates="select-options-e0b28efa-e286-cc08-794d-2e99779c0a84" value="Choose your option">
                <ul id="select-options-e0b28efa-e286-cc08-794d-2e99779c0a84" class="dropdown-content select-dropdown" style="width: 680px; position: absolute; top: 0px; left: 0px; opacity: 1; display: none;">
                    <li class="disabled"><span>Choose your option</span></li><li class=""><span>Option 1</span></li><li class=""><span>Option 2</span></li>
                    <li class=""><span>Option 3</span></li></ul><select class="initialized">
                    <option value="" disabled="" selected="">Choose your option</option>
                    <option value="1">Option 1</option>
                    <option value="2">Option 2</option>
                    <option value="3">Option 3</option>
                </select></div>
        </div>
    </div>


    <?php // echo $form->field($model, 'status', ['options' => ['class' => 'input-field']]) ?>

    <div class="form-group field-datasearch-geolocation input-field">
        <label class="control-label" for="datasearch-geolocation">Geolocation</label>
        <input type="text" id="datasearch-geolocation" class="form-control" name="DataSearch[geolocation]" value="">

        <div class="help-block"></div>
    </div>

    <div class="input-field col s12">
        <input id="email" type="email" class="validate">
        <label for="email" class="">Email</label>
    </div>

    <?php // echo $form->field($model, 'site') ?>

    <?php // echo $form->field($model, 'location') ?>

    <?php // echo $form->field($model, 'type') ?>

    <?php // echo $form->field($model, 'brand') ?>

    <?php // echo $form->field($model, 'model') ?>

    <?php // echo $form->field($model, 'horse_power') ?>

    <?php // echo $form->field($model, 'serial_no') ?>

    <?php // echo $form->field($model, 'kew_pa') ?>

    <?php // echo $form->field($model, 'floor') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <?php // echo $form->field($model, 'deleted') ?>

    <?php // echo $form->field($model, 'deleted_at')  ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
