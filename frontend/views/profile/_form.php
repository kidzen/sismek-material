<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\UserProfile */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-profile-form">
    <div class="row">
        <?php $form = ActiveForm::begin(); ?>
        <div class="input-field col s4">
            <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="input-field col s4">
            <?= $form->field($model, 'password_hash')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="input-field col s4">
            <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="input-field col s4">
            <?= $form->field($model, 'status')->dropDownList(['ACTIVE' => 'Aktif', 'INACTIVE' => 'Tidak Aktif']) ?>
        </div>
        <div class="input-field col s4">
            <div class="form-group field-userprofile-status">
            <select id="userprofile-status" class="form-control initialized" name="UserProfile[status]">
                <option value="ACTIVE">Aktif</option>
                <option value="INACTIVE">Tidak Aktif</option>
            </select>
            <label class="control-label" for="userprofile-status">Status</label>
            <div class="select-wrapper form-control initialized"><span class="caret">▼</span>
                <!--<input type="text" class="select-dropdown" readonly="true" data-activates="select-options-8ad630dd-9d6c-282a-a094-0fd620da90f2" value="Aktif"><ul id="select-options-8ad630dd-9d6c-282a-a094-0fd620da90f2" class="dropdown-content select-dropdown" style="width: 357px; position: absolute; top: 6.00001px; left: 12px; opacity: 1; display: none;"><li class=""><span>Aktif</span></li><li class=""><span>Tidak Aktif</span></li></ul>-->
            </div>

            <div class="help-block"></div>
        </div>        
        <!--</div>-->
    </div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
