<?php

use yii\helpers\Html;
use common\widgets\Menu2;
use common\widgets\Menu;
use common\widgets\MaterialWidget;
?>

<!-- START LEFT SIDEBAR NAV-->
<aside id="left-sidebar-nav">
    <ul id="slide-out" class="side-nav leftside-navigation">
        <li class="user-details cyan darken-2">
            <div class="row">
                <div class="col col s4 m4 l4">
                    <img src="<?= $assets ?>/images/avatar.jpg" alt="" class="circle responsive-img valign profile-image">
                </div>
                <div class="col col s8 m8 l8">
                    <ul id="profile-dropdown" class="dropdown-content">
                        <li><?= Html::a('<i class="mdi-action-face-unlock"></i> Profil', ['/profile']) ?></li>
                        <li><?= Html::a('<i class="mdi-communication-live-help"></i> Help', ['/site/help']) ?></li>
                        <li><?= Html::a('<i class="mdi-action-settings"></i> Aturan', ['/setting']) ?></li>
                        <li class="divider"></li>
                        <li><?= Html::a('<i class="mdi-hardware-keyboard-tab"></i> Log Keluar', ['/site/logout']) ?></li>
                    </ul>
                    <a class="btn-flat dropdown-button waves-effect waves-light white-text profile-btn" href="#" data-activates="profile-dropdown">AHMAD BIN ALI<i class="mdi-navigation-arrow-drop-down right"></i></a>
                    <p class="user-roal">Ketua Jabatan</p>
                </div>
            </div>
        </li>
        <li class="bold">
            <?= Html::a('<i class="mdi-action-dashboard"></i> Dashboard', ['/site/index'], ['class' => "waves-effect waves-cyan"]) ?>
        </li>

        <li class="no-padding">
            <ul class="collapsible collapsible-accordion">
                <li class="bold"><a class="collapsible-header waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> Permohonan</a>
                    <div class="collapsible-body">
                        <ul>
                            <li>
                                <?= Html::a('Pendaftaran Inventori', ['/site/index']) ?>
                            </li>
                            <li>
                                <!--<a href="layout-horizontal-menu.html">Penyelengaraan</a>-->
                                <?= Html::a('Penyelengaraan', ['/site/index']) ?>

                            </li>
                        </ul>
                    </div>
                </li>
            </ul>
        </li>
        <li class="bold"><a href="app-email.html" class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Mailbox <span class="new badge">4</span></a>
        </li>
        <li class="bold"><a href="app-calendar.html" class="waves-effect waves-cyan"><i class="mdi-editor-insert-invitation"></i> Jadual</a>
        </li>
        <li class="no-padding">
            <ul class="collapsible collapsible-accordion">
                <li class="bold"><a class="collapsible-header waves-effect waves-cyan"><i class="mdi-action-invert-colors"></i> Laporan</a>
                    <div class="collapsible-body">
                        <ul>
                            <li><?= Html::a('Bulanan', ['/site/index']) ?></li>
                            <li><?= Html::a('Tahunan', ['/site/index']) ?></li>
                            <li><?= Html::a('Kategori', ['/site/index']) ?></li>
                            <li><?= Html::a('Inventori', ['/site/index']) ?></li>
                        </ul>
                    </div>
                </li>
                <li class="bold"><a class="collapsible-header waves-effect waves-cyan"><i class="mdi-action-invert-colors"></i> Penyelengaraan</a>
                    <div class="collapsible-body">
                        <ul>
                            <li><?= Html::a('Bulanan', ['/site/index']) ?></li>
                            <li><?= Html::a('Tahunan', ['/site/index']) ?></li>
                            <li><?= Html::a('Dalam Pembaikian', ['/site/index']) ?></li>
                            <li><?= Html::a('Sejarah Pembaikian', ['/site/index']) ?></li>
                        </ul>
                    </div>
                </li>
                <li class="bold"><a href="app-widget.html" class="waves-effect waves-cyan"><i class="mdi-device-now-widgets"></i> Senarai Inventori</a>
                </li>
                <li class="bold"><a class="collapsible-header waves-effect waves-cyan"><i class="mdi-editor-insert-chart"></i> Analisis</a>
                    <div class="collapsible-body">
                        <ul>
                            <li><?= Html::a('Inventori', ['/site/index']) ?></li>
                            <li><?= Html::a('Bulanan', ['/site/index']) ?></li>
                            <li><?= Html::a('Aktif', ['/site/index']) ?></li>
                        </ul>
                    </div>
                </li>
            </ul>
        </li>
        <li class="li-hover"><div class="divider"></div></li>
        <li class="li-hover"><p class="ultra-small margin more-text">MORE</p></li>
        <li><?= Html::a('<i class="mdi-action-verified-user"></i> Pentadbiran Sistem  <span class="new badge"></span>', ['/site/index']) ?></li>
        <li><?= Html::a('<i class="mdi-image-grid-on"></i> Penyelengaraan Sistem', ['/site/index']) ?></li>
        <li><?= Html::a('<i class="mdi-action-swap-vert-circle"></i> Dokumentasi', ['/site/index']) ?></li>
        <li class="li-hover"><div class="divider"></div></li>
        <li><?= Html::a('<p class="ultra-small margin more-text">Laporan Hari Ini</p>', ['/site/index']) ?></li>
        <li class="li-hover">
            <div class="row">
                <div class="col s12 m12 l12">
                    <div class="sample-chart-wrapper">                            
                        <div class="ct-chart ct-golden-section" id="ct2-chart"></div>
                    </div>
                </div>
            </div>
        </li>
    </ul>
    <a href="#" data-activates="slide-out" class="sidebar-collapse btn-floating btn-medium waves-effect waves-light grey"><i class="mdi-navigation-menu"></i></a>
</aside>
<!-- END LEFT SIDEBAR NAV-->
