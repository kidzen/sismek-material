<?php

use yii\db\Migration;

class m170202_012819_data_injection extends Migration {

    public function up() {
        $sql = "insert into \"data\"
(select * from sismek.\"data\")
;
create view geosite as (
select 
  trim(both from substr(TRIM(TRAILING  from replace(\"site\",'. ','')),0,LENGTH(TRIM(TRAILING  from replace(\"site\",'. ',''))) - 5)) site
  ,trim(both from replace(replace(REGEXP_REPLACE(REGEXP_SUBSTR(\"site\", '\(.*.\)'), '(.)', '\1'),'(',''),')','')) \"short_code\"
  ,trim(both from \"geolocation\") \"geolocation\"
from \"data\"
);
insert into \"geolocation\"(\"short_code\",\"description\") 
(
select distinct
  geosite.\"short_code\",\"geolocation\" \"description\"
from geosite
);
";
        $sql2 = <<<SQL
insert into "data" (select * from sismek."data");
create or replace view "geosite" as (
    select 
      trim(both from substr(TRIM(TRAILING  from replace("site",'. ','')),0,LENGTH(TRIM(TRAILING  from replace("site",'. ',''))) - 5)) "site"
      ,TRIM(BOTH ')' FROM TRIM(BOTH '(' FROM REGEXP_SUBSTR("site", '\(.*.\)'))) "short_code"
      ,trim(both from "geolocation") "geolocation"
    from "data"
);
insert into "geolocation"("short_code","description") 
(
    select distinct
      "geosite"."short_code","geolocation" "description"
    from "geosite"
);

SQL;
        $sqlArray = explode(';', $sql2);
        foreach ($sqlArray as $query) {
//            $command = \Yii::$app->db->createCommand($query);
//            $this->execute($command);
//            $this->execute($query);
        }
    }

    public function down() {
        $sql2 = <<<SQL
drop view "geosite";
SQL;
        $sqlArray = explode(';', $sql2);
        foreach ($sqlArray as $query) {
//            $command = \Yii::$app->db->createCommand($query);
//            $this->execute($command);
//            $this->execute($query);
        }
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
