<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "data".
 *
 * @property string $id
 * @property string $geolocation
 * @property string $site
 * @property string $location
 * @property string $type
 * @property string $brand
 * @property string $model
 * @property string $horse_power
 * @property string $serial_no
 * @property string $kew_pa
 * @property string $floor
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $created_by
 * @property string $updated_by
 * @property integer $deleted
 * @property string $deleted_at
 */
class Data extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'data';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'number'],
            [['status', 'deleted'], 'integer'],
            [['created_at', 'updated_at', 'created_by', 'updated_by', 'deleted_at'], 'safe'],
            [['geolocation', 'site', 'location', 'type', 'brand', 'model', 'horse_power', 'serial_no', 'kew_pa', 'floor'], 'string', 'max' => 255],
            [['id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'geolocation' => 'Geolocation',
            'site' => 'Site',
            'location' => 'Location',
            'type' => 'Type',
            'brand' => 'Brand',
            'model' => 'Model',
            'horse_power' => 'Horse Power',
            'serial_no' => 'Serial No',
            'kew_pa' => 'Kew Pa',
            'floor' => 'Floor',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'deleted' => 'Deleted',
            'deleted_at' => 'Deleted At',
        ];
    }
}
