<?php

namespace common\widgets;

use Yii;
use common\widgets\MaterialWidget;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Html;

/**
 * Alert widget renders a message from session flash. All flash messages are displayed
 * in the sequence they were assigned using setFlash. You can set message as following:
 *
 * ```php
 * Yii::$app->session->setFlash('error', 'This is the message');
 * Yii::$app->session->setFlash('success', 'This is the message');
 * Yii::$app->session->setFlash('info', 'This is the message');
 * ```
 *
 * Multiple messages could be set as follows:
 *
 * ```php
 * Yii::$app->session->setFlash('error', ['Error 1', 'Error 2']);
 * ```
 *
 * @author Kartik Visweswaran <kartikv2@gmail.com>
 * @author Alexander Makarov <sam@rmcreative.ru>
 */
class CardStats extends MaterialWidget {

    public $primaryColor;
    public $contraColor;
    public $pluginId;
    public $icons;
    public $title;
    public $stats;
    public $type;
    public $comparatorTextColor;
    public $compareStats;
    public $compareStatement;
    public $plugins;
    public $template;

    public function init() {
        parent::init();
        $this->plugins = <<<HTML
                    <div class="card-action  $this->primaryColor darken-2">
                        <div id="$this->pluginId" class="center-align"></div>
                    </div>
HTML;
        $this->template = <<<HTML
             <div class="card">
                    <div class="card-content  $this->primaryColor $this->contraColor-text">
                        <p class="card-stats-title"><i class="$this->icons"></i> $this->title</p>
                        <h4 class="card-stats-number">$this->stats</h4>
                        <p class="card-stats-compare"><i class="mdi-hardware-keyboard-arrow-$this->type"></i> $this->compareStats <span class="$this->comparatorTextColor-text text-lighten-5">$this->compareStatement</span>
                        </p>
                    </div>
                        $this->plugins
                </div>
HTML;
    }

    public function run() {
        return $this->template;
    }

}
