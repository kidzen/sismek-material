<?php

namespace common\widgets;

use Yii;
use common\widgets\MaterialWidget;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Html;

/**
 * Alert widget renders a message from session flash. All flash messages are displayed
 * in the sequence they were assigned using setFlash. You can set message as following:
 *
 * ```php
 * Yii::$app->session->setFlash('error', 'This is the message');
 * Yii::$app->session->setFlash('success', 'This is the message');
 * Yii::$app->session->setFlash('info', 'This is the message');
 * ```
 *
 * Multiple messages could be set as follows:
 *
 * ```php
 * Yii::$app->session->setFlash('error', ['Error 1', 'Error 2']);
 * ```
 *
 * @author Kartik Visweswaran <kartikv2@gmail.com>
 * @author Alexander Makarov <sam@rmcreative.ru>
 */
class Collection extends MaterialWidget {

    public $widgetClass;
    public $widgetName;
    public $widgetOptions;
    public $template;
    public $return;

    public function init() {
        parent::init();
        $this->template = <<<HTML
<div class="col s12 m12 l4">
                <ul id="task-card" class="collection with-header">
                    <li class="collection-header cyan">
                        <h4 class="task-card-title">Perlu Selengara</h4>
                        <p class="task-card-date">Disember 26, 2015</p>
                    </li>
                    <li class="collection-item dismissable" style="touch-action: pan-y; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                        <input type="checkbox" id="task1">
                        <label for="task1" style="text-decoration: none;">Aircond 1. <a href="#" class="secondary-content"><span class="ultra-small">Today</span></a>
                        </label>
                        <span class="task-cat teal">Aircond</span>
                    </li>
                    <li class="collection-item dismissable" style="touch-action: pan-y; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                        <input type="checkbox" id="task2">
                        <label for="task2" style="text-decoration: none;">Aircond 5. <a href="#" class="secondary-content"><span class="ultra-small">Monday</span></a>
                        </label>
                        <span class="task-cat purple">Selengara</span>
                    </li>
                    <li class="collection-item dismissable" style="touch-action: pan-y; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                        <input type="checkbox" id="task3" checked="checked">
                        <label for="task3" style="text-decoration: line-through;">Aircond 2. <a href="#" class="secondary-content"><span class="ultra-small">Wednesday</span></a>
                        </label>
                        <span class="task-cat pink">Perlu Perhatian</span>
                    </li>
                    <li class="collection-item dismissable" style="touch-action: pan-y; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                        <input type="checkbox" id="task4" checked="checked" disabled="disabled">
                        <label for="task4" style="text-decoration: line-through;">Aircond 3</label>
                        <span class="task-cat cyan">Selesai Selengara</span>
                    </li>
                </ul>
            </div>
HTML;
    }

    public function run() {
        return $this->template;
    }

}
