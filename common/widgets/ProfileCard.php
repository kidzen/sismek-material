<?php

namespace common\widgets;

use Yii;
use common\widgets\MaterialWidget;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Html;

/**
 * Alert widget renders a message from session flash. All flash messages are displayed
 * in the sequence they were assigned using setFlash. You can set message as following:
 *
 * ```php
 * Yii::$app->session->setFlash('error', 'This is the message');
 * Yii::$app->session->setFlash('success', 'This is the message');
 * Yii::$app->session->setFlash('info', 'This is the message');
 * ```
 *
 * Multiple messages could be set as follows:
 *
 * ```php
 * Yii::$app->session->setFlash('error', ['Error 1', 'Error 2']);
 * ```
 *
 * @author Kartik Visweswaran <kartikv2@gmail.com>
 * @author Alexander Makarov <sam@rmcreative.ru>
 */
class ProfileCard extends MaterialWidget {

    public $primaryColor;
    public $contraColor;
    public $pluginId;
    public $icons;
    public $title;
    public $stats;
    public $type;
    public $comparatorTextColor;
    public $compareStats;
    public $compareStatement;
    public $plugins;
    public $template;

    public function init() {
        parent::init();
        $this->template = <<<HTML
            <div id="profile-card" class="card">
                    <div class="card-image waves-effect waves-block waves-light">
                        <img class="activator" src="/sismek1/frontend/web/assets/2fae13db/images/user-bg.jpg" alt="user background">
                    </div>
                    <div class="card-content">
                        <img src="/sismek1/frontend/web/assets/2fae13db/images/avatar.jpg" alt="" class="circle responsive-img activator card-profile-image">
                        <a class="btn-floating activator btn-move-up waves-effect waves-light darken-2 right">
                            <i class="mdi-action-account-circle"></i>
                        </a>

                        <span class="card-title activator grey-text text-darken-4">Penguna 3</span>
                        <p><i class="mdi-action-perm-identity cyan-text text-darken-2"></i> Ketua Jabatan</p>
                        <p><i class="mdi-action-perm-phone-msg cyan-text text-darken-2"></i> +(60) 13 987 6543</p>
                        <p><i class="mdi-communication-email cyan-text text-darken-2"></i> mail@mpsp.com</p>

                    </div>
                    <div class="card-reveal">
                        <span class="card-title grey-text text-darken-4">Nama KJ <i class="mdi-navigation-close right"></i></span>
                        <p>Jabatan 1.</p>
                        <p><i class="mdi-action-perm-identity cyan-text text-darken-2"></i> Ketua Jabatan</p>
                        <p><i class="mdi-action-perm-phone-msg cyan-text text-darken-2"></i> +(60) 13 987 6543</p>
                        <p><i class="mdi-communication-email cyan-text text-darken-2"></i> mail@domain.com</p>
                        <p><i class="mdi-social-cake cyan-text text-darken-2"></i> 18 Jun 1990</p>
                        <!--<p><i class="mdi-device-airplanemode-on cyan-text text-darken-2"></i> BAR - AUS</p>-->
                    </div>
                </div>
HTML;
    }

    public function run() {
        return $this->template;
    }

}
